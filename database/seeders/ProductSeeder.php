<?php

namespace Database\Seeders;

use App\Models;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    public function run(): void
    {
        $users = Models\User::inRandomOrder()->get();
        $categories = Models\Category::inRandomOrder()->get();

        foreach ($users as $user) {
            $productCount = rand(1, 3);
            $randomCategory = $categories->random();

            Models\Product::factory()->count($productCount)->create([
                'user_id' => $user->id,
                'category_id' => $randomCategory->id,
            ]);
        }
    }
}
