<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->id()
                ->comment('Category id');

            $table->timestamps();

            $table->string('name')
                ->unique()
                ->comment('Name of the category');
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('categories');
    }
};
