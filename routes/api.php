<?php

use App\Http\Controllers\Api as Controllers;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ProductController;
use Illuminate\Support\Facades\Route;

Route::prefix('/auth')->name('auth.')->controller(Controllers\AuthController::class)->group(function () {

    Route::post('/register', 'register')->name('register');

    Route::post('/login', 'login')->name('login');

    Route::post('/forgot-password', 'forgotPassword')->name('forgot.password');

    Route::post('/reset-password', 'resetPassword')->name('password.reset');

});

Route::prefix('/users')->name('users.')->middleware('auth:api')->controller(Controllers\UserController::class)->group(function () {

    Route::get('/me', 'getCurrentUser')->name('me');

    Route::get('/products', 'getMyProducts')->name('products');

    Route::get('/{userId}/products', 'getUserProducts')->name('user.products')->whereNumber('userId');

});

Route::prefix('/categories')->name('categories.')->middleware('auth:api')->controller(CategoryController::class)->group(function () {

    Route::get('/', 'getAllCategories')->name('all_categories');

    Route::get('/{categoryId}', 'getCategoryById')->name('category')->whereNumber('categoryId');

    Route::get('/{categoryId}/products', 'getProductsByCategory')->name('category_products')->whereNumber('categoryId');

    Route::post('/', 'createCategory')->name('create_category');

    Route::patch('/{categoryId}', 'updateCategory')->name('update_category')->whereNumber('categoryId');

    Route::delete('/{categoryId}', 'deleteCategory')->name('destroy_category')->whereNumber('categoryId');
});

Route::prefix('/products')->name('products.')->middleware('auth:api')->controller(ProductController::class)->group(function () {

    Route::get('/', 'getAllProducts')->name('all_products');

    Route::get('/{productId}', 'getProductById')->name('product')->whereNumber('productId');

    Route::post('/', 'createProduct')->name('create_product');

    Route::patch('/{productId}', 'updateProduct')->name('update_product')->whereNumber('productId');

    Route::delete('/{productId}', 'deleteProduct')->name('destroy_product')->whereNumber('productId');
});
