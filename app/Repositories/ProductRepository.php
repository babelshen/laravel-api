<?php

namespace App\Repositories;

use App\Http\Requests\Product as ProductRequests;
use App\Models\Product;
use App\Repositories\Interfaces\ProductRepositoryInterface;
use Illuminate\Support\Facades\Auth;

class ProductRepository implements ProductRepositoryInterface
{
    public function getAllProducts()
    {
        return Product::all();
    }

    public function getProductById(int $productId)
    {
        return Product::find($productId);
    }

    public function createProduct(ProductRequests\ProductCreateRequest $data)
    {
        $currentUser = Auth::user();

        return Product::create([
            'title' => $data->title,
            'description' => $data->description,
            'category_id' => $data->category_id,
            'user_id' => $currentUser->id,
        ]);
    }

    public function updateProduct(ProductRequests\ProductUpdateRequest $data, int $productId)
    {
        $product = Product::find($productId);

        if ($product) {
            $product->update([
                'title' => $data->title ?? $product->title,
                'description' => $data->description ?? $product->description,
                'category_id' => $data->category_id ?? $product->category_id,
            ]);

            return Product::find($productId);
        } else {
            throw new \Exception('Product not found', 404);
        }
    }

    public function deleteProduct(int $productId)
    {
        $product = Product::find($productId);
        if ($product) {
            return $product->delete();
        } else {
            throw new \Exception('Product not found', 404);
        }
    }
}
