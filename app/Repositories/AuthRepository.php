<?php

namespace App\Repositories;

use App\Http\Requests\Auth as AuthRequests;
use App\Models\User;
use App\Repositories\Interfaces\AuthRepositoryInterface;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;

class AuthRepository implements AuthRepositoryInterface
{
    public function register(AuthRequests\RegisterRequest $data)
    {
        $isUserExists = User::where('email', $data->email)->exists();

        if ($isUserExists) {
            throw new \Exception('User with email already exists', 400);
        }

        $user = User::create([
            'name' => $data->name,
            'email' => $data->email,
            'password' => $data->password,
            'email_verified_at' => now(),
        ]);

        return $user->createToken('default')->plainTextToken;
    }

    public function login(AuthRequests\LoginRequest $data)
    {
        $user = User::where('email', $data->email)->first();

        if (! $user) {
            throw new \Exception('User doesn`t exist', 400);
        }

        if (! Hash::check($data->password, $user->password)) {
            throw new \Exception('Wrong password', 400);
        }

        return $user->createToken('default')->plainTextToken;
    }

    public function forgotPassword(AuthRequests\ForgotPasswordRequest $data)
    {
        $response = Password::sendResetLink(
            $data->only('email')
        );

        if ($response === Password::RESET_LINK_SENT) {
            return DB::table('password_reset_tokens')
                ->where('email', $data->email)
                ->latest()
                ->first()
                ->token;
        } else {
            throw new \Exception('User with this e-mail not found', 404);
        }
    }

    public function resetPassword(AuthRequests\ResetPasswordRequest $data)
    {
        $token = $data->token;

        $passwordReset = DB::table('password_reset_tokens')
            ->where('token', $token)
            ->first();

        if (! $passwordReset) {
            throw new \Exception('Invalid reset password token', 422);
        }

        $user = User::where('email', $passwordReset->email)->first();

        if (! $user) {
            throw new \Exception('User not found', 404);
        } else {
            User::where('email', $passwordReset->email)->update(['password' => Hash::make($data->password)]);
        }

        DB::table('password_reset_tokens')
            ->where('email', $passwordReset->email)
            ->delete();
    }
}
