<?php

namespace App\Repositories\Interfaces;

use App\Http\Requests\Product as ProductRequests;

interface ProductRepositoryInterface
{
    public function getAllProducts();

    public function getProductById(int $id);

    public function createProduct(ProductRequests\ProductCreateRequest $data);

    public function updateProduct(ProductRequests\ProductUpdateRequest $data, int $id);

    public function deleteProduct(int $id);
}
