<?php

namespace App\Repositories\Interfaces;

use App\Http\Requests\Category\CategoryRequest;

interface CategoryRepositoryInterface
{
    public function getAllCategories();

    public function getCategoryById(int $id);

    public function getProductsByCategory(int $id);

    public function createCategory(CategoryRequest $data);

    public function updateCategory(CategoryRequest $data, int $id);

    public function deleteCategory(int $id);
}
