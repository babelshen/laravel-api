<?php

namespace App\Repositories\Interfaces;

use App\Http\Requests\Auth as AuthRequests;

interface AuthRepositoryInterface
{
    public function register(AuthRequests\RegisterRequest $data);

    public function login(AuthRequests\LoginRequest $data);

    public function forgotPassword(AuthRequests\ForgotPasswordRequest $data);

    public function resetPassword(AuthRequests\ResetPasswordRequest $request);
}
