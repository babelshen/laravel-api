<?php

namespace App\Repositories\Interfaces;

interface UserRepositoryInterface
{
    public function getUserProducts(int $userId);
}
