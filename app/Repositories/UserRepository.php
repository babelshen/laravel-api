<?php

namespace App\Repositories;

use App\Models\User;
use App\Repositories\Interfaces\UserRepositoryInterface;

class UserRepository implements UserRepositoryInterface
{
    public function getUserProducts(int $userId)
    {
        $user = User::find($userId);

        if ($user) {
            return $user->products;
        } else {
            throw new \Exception('User not found', 404);
        }
    }
}
