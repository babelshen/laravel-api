<?php

namespace App\Repositories;

use App\Http\Requests\Category\CategoryRequest;
use App\Models\Category;
use App\Repositories\Interfaces\CategoryRepositoryInterface;

class CategoryRepository implements CategoryRepositoryInterface
{
    public function getAllCategories()
    {
        return Category::all();
    }

    public function getCategoryById(int $categoryId)
    {
        return Category::find($categoryId);
    }

    public function getProductsByCategory(int $categoryId)
    {
        $category = Category::find($categoryId);

        if (! $category) {
            throw new \Exception('Category not found', 404);
        }

        return $category->products()->get();
    }

    public function createCategory(CategoryRequest $data)
    {
        return Category::create([
            'name' => $data->name,
        ]);
    }

    public function updateCategory(CategoryRequest $data, int $categoryId)
    {
        $category = Category::find($categoryId);

        if ($category) {
            $category->update([
                'name' => $data->name,
            ]);

            return Category::find($categoryId);
        } else {
            throw new \Exception('Category not found', 404);
        }
    }

    public function deleteCategory(int $categoryId)
    {
        $category = Category::find($categoryId);
        if ($category) {
            return $category->delete();
        } else {
            throw new \Exception('Category not found', 404);
        }
    }
}
