<?php

namespace App\Providers;

use App\Repositories as Repositories;
use App\Repositories\Interfaces as Interfaces;
use App\Services as Services;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    public function register(): void
    {

        $this->app->bind(Interfaces\AuthRepositoryInterface::class, Repositories\AuthRepository::class);
        $this->app->bind(Services\AuthService::class, function ($app) {
            return new Services\AuthService($app->make(Interfaces\AuthRepositoryInterface::class));
        });

        $this->app->bind(Interfaces\UserRepositoryInterface::class, Repositories\UserRepository::class);
        $this->app->bind(Services\UserService::class, function ($app) {
            return new Services\UserService($app->make(Interfaces\UserRepositoryInterface::class));
        });

        $this->app->bind(Interfaces\CategoryRepositoryInterface::class, Repositories\CategoryRepository::class);
        $this->app->bind(Services\CategoryService::class, function ($app) {
            return new Services\CategoryService($app->make(Interfaces\CategoryRepositoryInterface::class));
        });

        $this->app->bind(Interfaces\ProductRepositoryInterface::class, Repositories\ProductRepository::class);
        $this->app->bind(Services\ProductService::class, function ($app) {
            return new Services\ProductService($app->make(Interfaces\ProductRepositoryInterface::class));
        });

    }

    public function boot(): void
    {

    }
}
