<?php

namespace App\Http\Requests\Product;

use Illuminate\Foundation\Http\FormRequest;

class ProductUpdateRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'title' => [
                'nullable',
                'string',
                'unique:products,title',
                'min:3',
                'max:20',
            ],
            'description' => [
                'nullable',
                'string',
                'min:5',
                'max:100',
            ],
            'category_id' => [
                'nullable',
                'exists:categories,id',
            ],
        ];
    }
}
