<?php

namespace App\Http\Requests\Product;

use Illuminate\Foundation\Http\FormRequest;

class ProductCreateRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'title' => [
                'required',
                'string',
                'unique:products,title',
                'min:3',
                'max:20',
            ],
            'description' => [
                'required',
                'string',
                'min:5',
                'max:100',
            ],
            'category_id' => [
                'nullable',
                'exists:categories,id',
            ],
        ];
    }
}
