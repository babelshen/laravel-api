<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth as AuthRequests;
use App\Services\AuthService;

/**
 * @OA\Post(
 *      path="/api/auth/register",
 *      summary="Registration",
 *      tags={"Auth"},
 *
 *      @OA\RequestBody(
 *
 *         @OA\MediaType(
 *             mediaType="application/json",
 *
 *             @OA\Schema(
 *
 *                 @OA\Property(
 *                     property="name",
 *                     type="string"
 *                 ),
 *                 @OA\Property(
 *                     property="email",
 *                     type="string"
 *                 ),
 *                  @OA\Property(
 *                     property="password",
 *                     type="string"
 *                 ),
 *                 example={"name": "User name", "email": "useraddress@gmail.com", "password":"password123"}
 *             )
 *         )
 *     ),
 *
 *      @OA\Response(
 *          response=201,
 *          description="Success",
 *
 *          @OA\JsonContent(
 *
 *              @OA\Property(property="success", type="boolean", example="true"),
 *              @OA\Property(property="data", type="object",
 *                  @OA\Property(property="token", type="string", example="6|6OVYnQz9AqmUmlI1AJRA5zN69WCfi9TRNh5Ud70cd81455a1"),
 *              ),
 *          ),
 *      ),
 *
 *      @OA\Response(
 *          response=400,
 *          description="Bad request | Duplicate email",
 *
 *          @OA\JsonContent(
 *
 *              @OA\Examples(example="Result", value={"success": false, "message": "User with email already exists"}, summary="Result"),
 *          ),
 *      ),
 *
 *      @OA\Response(
 *          response=422,
 *          description="Validation error",
 *
 *          @OA\JsonContent(
 *
 *              @OA\Examples(example="Result", value={"message": "The name field must be a string.","errors": {"name": "The name field must be a string."}}, summary="Result"),
 *          ),
 *      ),
 * ),
 *
 * @OA\Post(
 *      path="/api/auth/login",
 *      summary="Login",
 *      tags={"Auth"},
 *
 *      @OA\RequestBody(
 *
 *         @OA\MediaType(
 *             mediaType="application/json",
 *
 *             @OA\Schema(
 *
 *                 @OA\Property(
 *                     property="email",
 *                     type="string"
 *                 ),
 *                  @OA\Property(
 *                     property="password",
 *                     type="string"
 *                 ),
 *                 example={"email": "useraddress@gmail.com", "password":"password123"}
 *             )
 *         )
 *     ),
 *
 *      @OA\Response(
 *          response=201,
 *          description="Success",
 *
 *          @OA\JsonContent(
 *
 *              @OA\Property(property="success", type="boolean", example="true"),
 *              @OA\Property(property="data", type="object",
 *                  @OA\Property(property="token", type="string", example="6|6OVYnQz9AqmUmlI1AJRA5zN69WCfi9TRNh5Ud70cd81455a1"),
 *              ),
 *          ),
 *      ),
 *
 *      @OA\Response(
 *          response=400,
 *          description="Bad request | Non correct password",
 *
 *          @OA\JsonContent(
 *
 *              @OA\Examples(example="Result", value={"success": false, "message": "User doesn`t exist"}, summary="Result"),
 *          ),
 *      ),
 *
 *      @OA\Response(
 *          response=422,
 *          description="Validation error",
 *
 *          @OA\JsonContent(
 *
 *              @OA\Examples(example="Result", value={"message": "The email field is required.","errors": {"email": "The email field is required."}}, summary="Result"),
 *          ),
 *      ),
 * ),
 *
 * @OA\Post(
 *      path="/api/auth/forgot-password",
 *      summary="Forgot password",
 *      tags={"Auth"},
 *
 *      @OA\RequestBody(
 *
 *         @OA\MediaType(
 *             mediaType="application/json",
 *
 *             @OA\Schema(
 *
 *                 @OA\Property(
 *                     property="email",
 *                     type="string"
 *                 ),
 *                 example={"email": "useraddress@gmail.com"}
 *             )
 *         )
 *     ),
 *
 *      @OA\Response(
 *          response=201,
 *          description="Success",
 *
 *          @OA\JsonContent(
 *
 *              @OA\Property(property="success", type="boolean", example="true"),
 *              @OA\Property(property="data", type="object",
 *                  @OA\Property(property="token", type="string", example="6|6OVYnQz9AqmUmlI1AJRA5zN69WCfi9TRNh5Ud70cd81455a1"),
 *              ),
 *              @OA\Property(property="message", type="string", example="Instructions to reset your password have been sent to your email"),
 *          ),
 *      ),
 *
 *      @OA\Response(
 *          response=404,
 *          description="User not found",
 *
 *          @OA\JsonContent(
 *
 *              @OA\Examples(example="Result", value={"success": false, "message": "User with this e-mail not found"}, summary="Result"),
 *          ),
 *      ),
 *
 *      @OA\Response(
 *          response=422,
 *          description="Validation error",
 *
 *          @OA\JsonContent(
 *
 *              @OA\Examples(example="Result", value={"message": "The email field is required.","errors": {"email": "The email field is required."}}, summary="Result"),
 *          ),
 *      ),
 * ),
 *
 * @OA\Post(
 *      path="/api/auth/reset-password",
 *      summary="Reset password",
 *      tags={"Auth"},
 *
 *      @OA\RequestBody(
 *
 *         @OA\MediaType(
 *             mediaType="application/json",
 *
 *             @OA\Schema(
 *
 *                 @OA\Property(
 *                     property="token",
 *                     type="string"
 *                 ),
 *                 @OA\Property(
 *                     property="password",
 *                     type="string"
 *                 ),
 *                 @OA\Property(
 *                     property="password_confirmation",
 *                     type="string"
 *                 ),
 *                 example={"token": "$2y$12$4NJIyeZH/IaniYSVPqRBuePUJRSWdNepuFaLiUpHXylj19cuJsC/e", "password": "password123P", "password_confirmation": "password123P"}
 *             )
 *         )
 *     ),
 *
 *      @OA\Response(
 *          response=200,
 *          description="Success",
 *
 *          @OA\JsonContent(
 *
 *              @OA\Property(property="success", type="boolean", example="true"),
 *              @OA\Property(property="message", type="string", example="Your password has been reset successfully"),
 *          ),
 *      ),
 *
 *      @OA\Response(
 *          response=404,
 *          description="User not found",
 *
 *          @OA\JsonContent(
 *
 *              @OA\Examples(example="Result", value={"success": false, "message": "User with this e-mail not found"}, summary="Result"),
 *          ),
 *      ),
 *
 *      @OA\Response(
 *          response=422,
 *          description="Invalid reset password token | Validation error",
 *
 *          @OA\JsonContent(
 *
 *              @OA\Examples(example="Result", value={"success": false, "message": "Invalid reset password token"}, summary="Result"),
 *          ),
 *      ),
 * ),
 */
class AuthController extends Controller
{
    public function __construct(
        protected AuthService $authService
    ) {
    }

    public function register(AuthRequests\RegisterRequest $request)
    {
        try {
            $token = $this->authService->register($request);

            return response()->json([
                'success' => true,
                'data' => [
                    'token' => $token,
                ],
            ], 201);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'message' => $e->getMessage(),
            ], $e->getCode());
        }
    }

    public function login(AuthRequests\LoginRequest $request)
    {
        try {
            $token = $this->authService->login($request);

            return response()->json([
                'success' => true,
                'data' => [
                    'token' => $token,
                ],
            ], 201);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'message' => $e->getMessage(),
            ], $e->getCode());
        }
    }

    public function forgotPassword(AuthRequests\ForgotPasswordRequest $request)
    {
        try {
            $token = $this->authService->forgotPassword($request);

            return response()->json([
                'success' => true,
                'data' => [
                    'token' => $token,
                ],
                'message' => 'Instructions to reset your password have been sent to your email.',
            ], 201);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'message' => $e->getMessage(),
            ], $e->getCode());
        }
    }

    public function resetPassword(AuthRequests\ResetPasswordRequest $request)
    {
        try {
            $this->authService->resetPassword($request);

            return response()->json([
                'success' => true,
                'message' => 'Your password has been reset successfully.',
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'message' => $e->getMessage(),
            ], $e->getCode());
        }
    }
}
