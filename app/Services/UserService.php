<?php

namespace App\Services;

use App\Repositories\Interfaces\UserRepositoryInterface;

class UserService
{
    public function __construct(
        protected UserRepositoryInterface $userRepository
    ) {
    }

    public function getUserProducts(int $id)
    {
        return $this->userRepository->getUserProducts($id);
    }
}
