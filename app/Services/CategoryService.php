<?php

namespace App\Services;

use App\Repositories\Interfaces\CategoryRepositoryInterface;

class CategoryService
{
    public function __construct(
        protected CategoryRepositoryInterface $categoryRepository
    ) {
    }

    public function getAllCategories()
    {
        return $this->categoryRepository->getAllCategories();
    }

    public function getCategoryById($id)
    {
        return $this->categoryRepository->getCategoryById($id);
    }

    public function getProductsByCategory($id)
    {
        return $this->categoryRepository->getProductsByCategory($id);
    }

    public function createCategory($data)
    {
        return $this->categoryRepository->createCategory($data);
    }

    public function updateCategory($data, $id)
    {
        return $this->categoryRepository->updateCategory($data, $id);
    }

    public function deleteCategory(int $categoryId)
    {
        return $this->categoryRepository->deleteCategory($categoryId);
    }
}
