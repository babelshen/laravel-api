<?php

namespace App\Services;

use App\Http\Requests\Auth as AuthRequests;
use App\Repositories\Interfaces\AuthRepositoryInterface;

class AuthService
{
    public function __construct(
        protected AuthRepositoryInterface $authRepository
    ) {
    }

    public function register(AuthRequests\RegisterRequest $data)
    {
        return $this->authRepository->register($data);
    }

    public function login(AuthRequests\LoginRequest $data)
    {
        return $this->authRepository->login($data);
    }

    public function forgotPassword(AuthRequests\ForgotPasswordRequest $data)
    {
        return $this->authRepository->forgotPassword($data);
    }

    public function resetPassword(AuthRequests\ResetPasswordRequest $data)
    {
        return $this->authRepository->resetPassword($data);
    }
}
